import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javafx.scene.text.Font;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class ErsteGUI extends JFrame {

	private JPanel pnlBackground;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ErsteGUI frame = new ErsteGUI("Erste GUI");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ErsteGUI(String title) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle(title);
		setBounds(100, 100, 450, 621);
		pnlBackground = new JPanel();
		pnlBackground.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlBackground);
		pnlBackground.setLayout(null);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblDieserTextSoll.setHorizontalTextPosition(SwingConstants.LEADING);
		lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
		lblDieserTextSoll.setBounds(0, 51, 424, 14);
		pnlBackground.add(lblDieserTextSoll);
		
		JLabel lblAufgabeHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabeHintergrundfarbe.setBounds(10, 109, 185, 14);
		pnlBackground.add(lblAufgabeHintergrundfarbe);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRed_clicked();
			}
		});
		btnRot.setBounds(10, 134, 103, 23);
		pnlBackground.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGreen_clicked();
			}
		});
		btnGrn.setBounds(145, 134, 131, 23);
		pnlBackground.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlue_clicked();
			}
		});
		btnBlau.setBounds(300, 134, 124, 23);
		pnlBackground.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonYellow_clicked();
			}
		});
		btnGelb.setBounds(10, 168, 103, 23);
		pnlBackground.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandard_clicked();
			}
		});
		btnStandardfarbe.setBounds(145, 168, 131, 23);
		pnlBackground.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlBackground.setBackground(chooseColor());
			}
		});
		btnFarbeWhlen.setBounds(300, 168, 124, 23);
		pnlBackground.add(btnFarbeWhlen);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setBounds(10, 202, 146, 14);
		pnlBackground.add(lblAufgabeText);
		
		JButton btnNewButton = new JButton("Arial");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblDieserTextSoll.setFont(new Font("Arial", Font.BOLD, lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnNewButton.setBounds(10, 227, 103, 23);
		pnlBackground.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Comic Sans MS");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Comic Sans MS", Font.BOLD, lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnNewButton_1.setBounds(143, 227, 133, 23);
		pnlBackground.add(btnNewButton_1);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Courier New", Font.BOLD, lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnCourierNew.setBounds(300, 227, 124, 23);
		pnlBackground.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(10, 261, 414, 20);
		pnlBackground.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(10, 292, 185, 23);
		pnlBackground.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText("");
			}
		});
		btnTextImLabel.setBounds(223, 292, 201, 23);
		pnlBackground.add(btnTextImLabel);
		
		JLabel lblAufgabeSchrittfarbe = new JLabel("Aufgabe 3: Schrittfarbe \u00E4ndern");
		lblAufgabeSchrittfarbe.setBounds(10, 326, 185, 14);
		pnlBackground.add(lblAufgabeSchrittfarbe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.RED);
			}
		});
		btnRot_1.setBounds(10, 351, 103, 23);
		pnlBackground.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(145, 351, 131, 23);
		pnlBackground.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(300, 351, 124, 23);
		pnlBackground.add(btnSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setBounds(10, 385, 185, 14);
		pnlBackground.add(lblAufgabeSchriftgre);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String ausgangsFont = lblDieserTextSoll.getFont().getFontName();
				int ausgangsFontsize = lblDieserTextSoll.getFont().getSize();
				lblDieserTextSoll.setFont(new Font(ausgangsFont, Font.BOLD, ausgangsFontsize + 1));
				
			}
		});
		button.setBounds(10, 410, 185, 23);
		pnlBackground.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String ausgangsFont = lblDieserTextSoll.getFont().getFontName();
				int ausgangsFontsize = lblDieserTextSoll.getFont().getSize();
				if (ausgangsFontsize <= 0) {
					ausgangsFontsize = 1;
				}
				lblDieserTextSoll.setFont(new Font(ausgangsFont, Font.BOLD, ausgangsFontsize - 1));
			}
		});
		button_1.setBounds(223, 410, 201, 23);
		pnlBackground.add(button_1);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setBounds(10, 444, 185, 14);
		pnlBackground.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblDieserTextSoll.setHorizontalAlignment(2);
			}
		});
		btnLinksbndig.setBounds(10, 469, 103, 23);
		pnlBackground.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(0);
			}
		});
		btnZentriert.setBounds(145, 469, 131, 23);
		pnlBackground.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(4);
			}
		});
		btnRechtsbndig.setBounds(300, 469, 124, 23);
		pnlBackground.add(btnRechtsbndig);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setBounds(10, 503, 185, 14);
		pnlBackground.add(lblAufgabeProgramm);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(10, 528, 414, 41);
		pnlBackground.add(btnExit);
	}


	public Color chooseColor() {
		return JColorChooser.showDialog(this, "WFarbe", Color.white);
	}

	public void buttonStandard_clicked() {
		this.pnlBackground.setBackground(Color.WHITE);
	}

	public void buttonYellow_clicked() {
		this.pnlBackground.setBackground(Color.YELLOW);
	}

	public void buttonBlue_clicked() {
		this.pnlBackground.setBackground(Color.BLUE);
	}

	public void buttonGreen_clicked() {
		this.pnlBackground.setBackground(Color.GREEN);
	}

	public void buttonRed_clicked() {
		this.pnlBackground.setBackground(Color.RED);
	}
}
