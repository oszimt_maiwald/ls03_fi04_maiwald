import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JSlider;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.text.JTextComponent;

public class TestGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JTextField txtNachname;
	private JTextField txtEmail;
	private JTextField txtGeburtsdatum;
	private JButton btnRegistrieren;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestGUI frame = new TestGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 570, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Registrierung");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
		contentPane.add(lblNewLabel);
		
		txtUsername = new JTextField();
		txtUsername.setHorizontalAlignment(SwingConstants.LEFT);
		txtUsername.setText("Vorname");
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		txtNachname = new JTextField();
		txtNachname.setText("Nachname");
		contentPane.add(txtNachname);
		txtNachname.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setText("E-Mail");
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtGeburtsdatum = new JTextField();
		txtGeburtsdatum.setText("Geburtsdatum");
		contentPane.add(txtGeburtsdatum);
		txtGeburtsdatum.setColumns(10);
		
		btnRegistrieren = new JButton("Registrieren");
		btnRegistrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(txtUsername.getText());
				System.out.println(txtNachname.getText());
				System.out.println(txtEmail.getText());
				System.out.println(txtGeburtsdatum.getText());
			}
		});
		contentPane.add(btnRegistrieren);
	}
}
